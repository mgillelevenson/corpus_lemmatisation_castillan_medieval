# Manuel d'utilisation

L'idée est de profiter de notre travail passé avec O. Brisville-Fertin, M. Díez Yáñez et S. Gabay afin d'identifier plus aisément les points à corriger de Freeling, du point de vue non seulement des erreurs mais aussi des divergences linguistiques par rapport à l'outil existant. 

On utilise les indices de certitude qui sortent de Freeling pour identifier (avec un seuil assez élevé de 0.8 -- en dessous duquel la ligne est marquée comme à revoir dans la catégorie morph sur Pyrrha -- qui pourra être modifié au besoin) les analyses à vérifier.  


## Modification par rapport à EAGLES

- le plus que parfait
- l'impératif de 2e/3e personne (ou subjonctif à valeur impérative?): VMM01P0 et VMM03P0. Je corrige en subjonctif.

## Modifications par rapport à freeling:

- la crase : lemme1+lemme2 et les POS complets
- quier: CC
- adjectifs: AQ0FS0 ou AQ0FS00. Les deux analyses coexistent, la deuxième semble produite quand la forme est inconnue et
non lemmatisée (mais ça n'est pas systématique). Je m'en sers pour identifier ces formes à corriger.

## Doutes

- désambiguation sémantique ? don1, don2?


## À faire


(en cours) faudra imaginer un outil en ligne de commande qui permette de corriger en masse en fonction du contexte.

### Modifications en contexte

- si /  [por] sí
- si /  [para] sí
- [si] fuera / fuera [de]
- quel [verbe] / quel [NC]
- [haver] y / [V] y [V] / [NC] y [NC]

## Ciblage de correction


- désambiguisation essentiellement du POS (personnes, article/pronom)
- correction des problèmes de normalisation (imparfaits en -ava)
- adverbes en mientre/miente
- fuera / fuera
- y (5000 occurrences)
- subjonctif imparfait en -ra / plus que parfait



## Problèmes


régler les problèmes suivants: 

- allegar / llegar
- los godos / los reyes godos
- nom propre de Lieu, de personne = quand c'est en contexte de nom ou pas
