




def main():
    with open("../tokenized.lemmatized.tsv", "r") as input_file:
        as_list = [line.replace("\n", "") for line in input_file.readlines()]
    splitted_list = [line.split()[:4] for line in as_list]
    splitted_list = [line for line in splitted_list if line != []]
    # On va cibler les analyses incertaines pour faciliter la correction. Seuil fixé à 0.8
    mod_list = [[form, lemma, pos, "True"]  if float(prob) < 0.8 else [form, lemma, pos, "False"] for form, lemma, pos, prob in splitted_list]

    # Maintenant on va isoler les analyses qui renvoient la forme exacte (parfois, des erreurs de lemmatisation)
    out_list = []
    debug_list = []
    for form, lemma, pos, check in mod_list:
        if form == lemma and check == "False":
            check = "True"
        out_list.append([form, lemma, pos, check])

    with open("../tokenized.lemmatized.clean.tsv", "w") as output_file:
        output_file.write("Form\tLemma\tPOS\tCheck\n")
        for line in out_list:
            output_file.write("\t".join(line) + "\n")


if __name__ == '__main__':
    main()