import csv
import json


def convert():
    out_list = []
    with open("../cordemaforo.csv", "r") as input_cordemaforo:
        as_list = [item.replace("\n", "") for item in input_cordemaforo.readlines()]
    headers = as_list[0].split("\t")
    for element in as_list[1:]:
        corrected_element = element.split("\t")
        as_dict = {headers[idx]: item for idx, item in enumerate(corrected_element)}
        out_list.append(as_dict)

    with open("cordemaforo.json", "w") as output_json:
        json.dump(out_list, output_json)


if __name__ == '__main__':
    convert()
