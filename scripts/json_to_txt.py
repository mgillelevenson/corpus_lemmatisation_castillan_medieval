import copy
import json
import glob
import os
import random
import string
import sys
import hashlib
import re
import stanza
stanza.download(lang="multilingual")
from stanza.models.common.doc import Document
from stanza.pipeline.core import Pipeline
from difflib import SequenceMatcher
from collections import OrderedDict

from Levenshtein import distance as levenshtein_distance

import networkx
from networkx.algorithms.components.connected import connected_components
import tqdm


def lang_detection(nlp, string):
    docs = Document([], text=string)
    nlp(docs)
    return docs.lang

def to_graph(l):  # https://stackoverflow.com/a/4843408
    graphed_list = networkx.Graph()
    for part in l:
        # each sublist is a bunch of nodes
        graphed_list.add_nodes_from(part)
        # it also imlies a number of edges:
        graphed_list.add_edges_from(to_edges(part))
    return graphed_list


def get_sentence_stats(sentence):
    words_length = [len(word) for word in sentence.split()]
    length_dict = OrderedDict()
    for word in words_length:
        try:
            length_dict[word] += 1
        except KeyError:
            length_dict[word] = 1


def compare_with_sets(list_of_texts):
    kept_ids = []
    seen = []
    for example, identifier in list_of_texts:
        if example in seen:
            pass
        else:
            seen.append(example)
            kept_ids.append(identifier)
    return kept_ids


def compare(list_of_textes: list):
    seen = []
    added = []
    similars = []
    text_list = []
    list_of_textes.sort(key=lambda x: x[1])
    list_of_textes = list(set(list_of_textes))
    list_of_textes.sort(key=lambda x: x[1])
    for index_a, (text_a, id_a) in enumerate(list_of_textes):
        for index_b, (text_b, id_b) in enumerate(list_of_textes):
            if (index_b, index_a) in seen or (index_a, index_b) in seen or index_b == index_a:
                continue
            seen.append((index_a, index_b))
            seen.append((index_b, index_a))
            similarity = similar(text_a, text_b)
            if similarity < .5:
                if index_b not in added:
                    text_list.append(text_b)
                    added.append(index_b)
                # Avec les couples de similaires on va les projeter dans un graphe et identifier les noeuds
                # qui sont reliés entre eux
            else:
                if (index_a, index_b) in similars:
                    pass
                elif (index_b, index_a) in similars:
                    pass
                else:
                    similars.append((index_a, index_b))

    # On regroupe tous les textes similaires
    graphed_list = to_graph(similars)
    cleaned_list = [list(item) for item in list(connected_components(graphed_list))]

    # On ne garde que le premier texte parmi tous les groupes de dupliqués
    to_keep = [element[0] for element in cleaned_list]
    all_duplicates = [element for sublist in cleaned_list for element in sublist]
    # On identifie les index des textes à ne pas conserver
    to_remove = [element for element in all_duplicates if element not in to_keep]

    # On crée une nouvelle liste qui ne garde pas les textes identifiés
    cleaned_unique_sentences = [id for index, (text, id) in enumerate(list_of_textes) if index not in to_remove]
    return cleaned_unique_sentences


def get_examples_from_json(dictionnary) -> dict:
    data = dictionnary
    examples = {}
    for identifier, work in data.items():
        examples[identifier] = []
        for example in work['examples']:
            examples[identifier].extend(example['tokenized-no-redundancy'])
    return examples


def get_word_count_from_json(dictionnary):
    data = dictionnary
    examples_count = 0
    WC = 0
    for identifier, work in data.items():
        for example in work['examples']:
            examples_count += len(example['tokenized-no-redundancy'])
            WC += example['words-tokenized']
    print(f"Total number of words: {WC}")
    print(f"Total number of sentences: {examples_count}")


def debug(message):
    print(debug_val)
    if debug_val:
        print(message)


def to_edges(l):  # https://stackoverflow.com/a/4843408
    """
        treat `l` as a Graph and returns it's edges
        to_edges(['a','b','c','d']) -> [(a,b), (b,c),(c,d)]
    """
    it = iter(l)
    last = next(it)

    for current in it:
        yield last, current
        last = current


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def clean(string):
    page_pattern = re.compile("\[\s?P[áa]gina[^]]*\s+\d+[A-Za-z]*\s?\]")
    page_pattern_2 = re.compile("\[\s?P[áa]gina[^]]*\s+[A-Z],?\s?F?O?L?\.?")
    accents_mapping = {"á": "a",
                       "é": "e",
                       "í": "i",
                       "ó": "o",
                       "ú": "u",
                       "ý": "y"}
    for orig, replacement in accents_mapping.items():
        string = string.replace(orig, replacement)

    add_pattern = re.compile("\(([^)(]*)\)")
    fol_pattern = re.compile("\[?f?o?l?\.?\s?\d+[vrRV]\s?\]")
    output_string = re.sub(page_pattern, "", string)
    output_string = re.sub(add_pattern, r"\g<1>", output_string)
    output_string = re.sub(fol_pattern, r"", output_string)
    output_string = re.sub(r'\s+', " ", output_string)
    output_string = re.sub(page_pattern_2, " ", output_string)
    output_string = output_string.replace("*", "")
    output_string = output_string.replace("[", "")
    output_string = output_string.replace("]", "")
    output_string = output_string.replace("[ Pagina FOL.", "")
    return output_string.strip()


def tokenize(string):
    punctuation = re.compile("([,;!:?¿)¡(])")
    output_string = re.sub(punctuation, r"\n\g<1>", string)
    output_string = output_string.replace(" ", "\n")
    output_string = output_string.replace(".", "\n.\n\n")
    return output_string


def round_to_multiple(number, multiple):
    number = int(number)
    closest_value = multiple * round(number / multiple)
    if closest_value > number:
        correct_value = closest_value - multiple
    else:
        correct_value = closest_value
    return correct_value


def remove_small_works(corpus: dict, threshold: int):
    # Cette fonction permet de supprimer les oeuvres dont le nombre de mot est inférieur à un certain seuil
    reduced_dictionnary = copy.deepcopy(corpus)
    reduced_rate = 0
    for key, work in corpus.items():
        word_count = 0
        for example in work['examples']:
            word_count += example['words-tokenized']
        if word_count < threshold:
            reduced_rate += 1
            reduced_dictionnary.pop(key, None)

    with open(f"{out_dir}/reduced_corpus.json", "w") as output_json:
        json.dump(reduced_dictionnary, output_json)

    print(f"Reduced corpus length: {len(reduced_dictionnary)} works.")
    return reduced_dictionnary


def reduce_date_range(corpus: dict):
    # Cette fonction permet de supprimer les oeuvres antérieures à 1250, en utilisant la date normalisée
    output_dict = OrderedDict()
    for key, work in corpus.items():
        collection = []
        for example in work:
            if any(element in example['date_range'] for element in ["1500"]):
                pass
            else:
                collection.append(example)
        output_dict[key] = collection
    return output_dict


def reduce_by_century(corpus: dict, dictionnary: dict):
    # Cette fonction permet d'équilibrer le corpus en diminuant la quantité d'oeuvres par siècle siècle
    output_dict = dict()
    for key, work in corpus.items():
        collection = []
        examples = work['examples']
        work.pop('examples', None)
        for example in examples:
            random_integer = random.randint(1, 100)
            if any(century == work['century'] for century in dictionnary.keys()):
                if random_integer > dictionnary[work['century']]:
                    pass
                else:
                    collection.append(example)
        work['examples'] = collection
        output_dict[key] = work
    print("Done")
    with open("../test_reduced.json", "w") as output_json:
        json.dump(output_dict, output_json)
    return output_dict


def get_hash(dictionnary_or_list):
    print(hashlib.md5(json.dumps(dictionnary_or_list).encode("utf-8")).hexdigest())


def get_hash_text(filepath: str) -> None:
    with open(filepath, "r") as input_text_file:
        text_object = input_text_file.read()
    print(hashlib.md5(text_object.encode("utf-8")).hexdigest())


def create_date_range(date: str) -> str:
    # On va normaliser la date par cette fonction, pour pouvoir trier les entrées
    date = date.replace("a", "").replace("c", "").replace(" ", "").replace("p", "")
    if "-" not in date and "," not in date:
        minimal_milestone = round_to_multiple(date, 25)
    elif "," in date:
        minimal_milestone = round_to_multiple(date.split(",")[0], 25)
    else:
        # On décide ici de choisir la valeur la plus faible, arbitrairement.
        minimal_milestone = round_to_multiple(date.split("-")[0], 25)
    correct_date = f"{minimal_milestone}-{minimal_milestone + 24}"
    return correct_date


def create_century(date: str) -> str:
    # On va normaliser la date par cette fonction, pour pouvoir trier les entrées
    date = date.replace("a", "").replace("c", "").replace(" ", "").replace("p", "")
    if "-" not in date and "," not in date:
        norm_date = round_to_multiple(date, 100)
    elif "," in date:
        norm_date = round_to_multiple(date.split(",")[0], 100)
    else:
        # On décide ici de choisir la valeur la plus faible, arbitrairement.
        norm_date = round_to_multiple(date.split("-")[0], 100)
    # On convertit la date en siècle.
    century = str(int(norm_date) + 100)[:2]
    return century


def normalize_theme(input):
    # On va corriger un problème sur une des catégories
    input = input.replace("asc .tica", "ascética")
    code, subtheme = input.split(".")
    theme = corde_themes[code]

    return theme, subtheme


def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def create_main_json_file():
    full_corpus = dict()
    list_of_files = glob.glob(f"{in_dir}/json/*.json")
    print(f"{in_dir}/json/*.json")
    random.shuffle(list_of_files)
    for file in tqdm.tqdm(list_of_files):
        try:
            data = json.load(open(file))
        except json.decoder.JSONDecodeError:
            continue

        for key, item in data.items():
            normalized_title = item["title"].replace(" ", "").replace("/", "")[:60:3].replace(".", "").replace(
                ",", "") + item["published-by"].replace(" ", "")[:60:3]
            try:
                full_corpus[normalized_title]
            except KeyError:
                item["date_range"] = create_date_range(item["year"])
                item["century"] = create_century(item["year"])
                item["theme"], item["subtheme"] = normalize_theme(item["theme"])

            current_example = {}
            current_example_text = clean(item["text"])
            current_example['id'] = str(get_random_string(10))
            current_example['text'] = current_example_text
            current_example["words"] = len(item["text"].split())
            try:
                existing_examples = full_corpus[normalized_title]['examples']
            except KeyError:
                existing_examples = []
            existing_examples.append(current_example)
            try:
                full_corpus[normalized_title]['examples'] = existing_examples
            except KeyError:
                full_corpus[normalized_title] = item
                full_corpus[normalized_title]['examples'] = existing_examples

            full_corpus[normalized_title].pop('text', None)

    with open(f"{out_dir}/full_corpus.json", "w") as output_dict:
        json.dump(full_corpus, output_dict)


def clean_dates(date):
    cleaned = date.replace("¿", "")
    cleaned = cleaned.replace("?", "")
    return cleaned


def first_reduction_step():
    """
    Cette fonction enlève les textes latins, ajoute les informations de date
    de CORDEMÁFORO
    :return: 
    """
    print("Reducing corpus")
    # full_corpus = OrderedDict()
    reduced_corpus = dict()

    with open("cordemaforo.json", "r") as output_json:
        cordemaforo_list = json.load(output_json)

    with open(f"{out_dir}/full_corpus.json", "r") as input_dict:
        full_corpus = json.load(input_dict)

    nlp = Pipeline(lang="multilingual", processors="langid")
    print(f"Orig corpus length: {len(full_corpus)}")
    out_corpus = {}
    for text_id, texts_same_author in tqdm.tqdm(full_corpus.items()):
        unique_sentences = []
        texts = []
        texts_to_compare = []
        all_examples = texts_same_author['examples']
        publisher = texts_same_author['published-by']
        publication_date = publisher.split(",")[-1].replace(" ", "")
        try:
            int(publication_date)
        except ValueError:
            publication_date = "0"
        correct_publisher = ", ".join(publisher.split(",")[:-1])
        texts_same_author['published-by'] = correct_publisher
        texts_same_author['publication-date'] = publication_date
        texts_same_author['edition-date-range'] = create_date_range(publication_date)
        witDate, witDateType, date_amended = get_correct_wit_date(texts_same_author, cordemaforo_list)
        texts_same_author['wit-date-type'] = witDateType
        texts_same_author['date-amended'] = date_amended
        witDate = clean_dates(witDate)
        if witDate != "*":
            wit_date_range = create_date_range(witDate)
            wit_date_century = create_century(witDate)
            texts_same_author['wit-date'] = witDate
            texts_same_author['wit-date-range'] = wit_date_range
            texts_same_author['wit-date-century'] = wit_date_century
        else:
            date = texts_same_author['year']
            wit_date_range = create_date_range(date)
            wit_date_century = create_century(date)
            texts_same_author['wit-date'] = date
            texts_same_author['wit-date-range'] = wit_date_range
            texts_same_author['wit-date-century'] = wit_date_century
            texts_same_author['date-amended'] = False
        random.shuffle(all_examples)
        for example in all_examples[:max(global_limit, madison_limit) + 10]:
            string = example["text"]
            sentence_id = example["id"]
            cleaned = clean(string)
            if wit_date_century == "13":
                # Pour les textes du 13e siècle on lance une identification de langue pour exclure le latin
                try:
                    lang = lang_detection(nlp, cleaned[200:500])
                except RuntimeError:
                    lang = "es"
                
                # On détecte deux fois: sur un fragment du texte, et on vérifie sur son intégralité
                if lang == "la":
                    lang = lang_detection(nlp, cleaned)
                    if lang == "la":
                        continue
            texts_to_compare.append((cleaned, sentence_id))
        # On réduit la taille des textes de Madison qui ne sont pas ponctués et qui posent beaucoup de problème de
        # comparaison.
        if len(texts_to_compare) > 1:
            if "Madison" not in texts_same_author["published-by"]:
                # Dans ce cas on va vérifier la stricte égalité des chaînes de caractère
                no_duplicates = compare_with_sets(texts_to_compare[:global_limit])
                texts.extend(no_duplicates)
            else:
                # Dans l'autre cas,
                no_duplicates = compare(texts_to_compare[:madison_limit])
                texts.extend(no_duplicates)
            without_duplicates = list(set(texts))
            unique_sentences.extend(without_duplicates)
            # Maintenant on va mettre à jour le dictionnaire en ne gardant que les phrases conservées
        else:
            unique_sentences.extend([sentence_id for sentence_id in texts])

        texts = []
        for sentence in texts_same_author['examples']:
            words = sentence["words"]
            sentence_id = sentence["id"]
            if sentence["id"] in unique_sentences:
                texts.append({'text': sentence['text'], 'id': sentence_id, 'words': words})
            else:
                pass
        texts_same_author['examples'] = texts
        if len(texts) != 0:
            out_corpus[text_id] = texts_same_author
            out_corpus[text_id].pop('text', None)

    with open(f"{out_dir}/merged_corpus.json", "w") as output_dict:
        json.dump(out_corpus, output_dict)


def balance_corpus():
    with open(f"{out_dir}/merged_corpus.json", "r") as input_dict:
        in_corpus = json.load(input_dict)
    reduced_corpus = reduce_by_century(in_corpus, reduce_dict)
    # reduced_corpus = reduce_date_range(reduced_corpus)
    reduced_corpus = reduce_by_subtheme(reduced_corpus, "Documentos notariales", documentos_notariales_ratio)
    reduced_corpus = reduce_by_subtheme(reduced_corpus, "Ordenamientos y codigos legales", codigos_legales_ratio)
    with open(f"{out_dir}/reduced_corpus.json", "w") as output_dict:
        json.dump(reduced_corpus, output_dict)
    return reduced_corpus


def reduce_by_subtheme(corpus, subtheme, ratio):
    # Cette fonction permet de supprimer les oeuvres dont le nombre de mot est inférieur à un certain seuil
    reduced_dictionnary = copy.deepcopy(corpus)
    for key, work in corpus.items():
        theme = work['subtheme']
        randint = random.random()
        if randint > ratio and subtheme in theme:
            reduced_dictionnary.pop(key, None)
        elif subtheme not in theme:
            pass
    return reduced_dictionnary


def create_txt_files(example_dict):
    with open(f"{out_dir}/corpus.txt", "w") as text_sentences:
        for id, examples in example_dict.items():
            text_sentences.write("\n" + f"[{id}]" + "\n")
            for example in examples:
                text_sentences.write(example + ".\n")
            text_sentences.write("\n\n")

    with open(f"{out_dir}/tokenized.txt", "w") as text_sentences:
        for id, examples in example_dict.items():
            text_sentences.write("\n[" + id + "]\n")
            # https://stackoverflow.com/a/1820377
            for sentence in examples:
                splitted_sentence = [token for token in re.compile(r'([\s!¿"¡:;,\.?])').split(sentence) if
                                     token not in ["", " "]]
                if not splitted_sentence:
                    continue
                for token in splitted_sentence:
                    text_sentences.write(f"{token}\n")
                if splitted_sentence[-1] not in [".", "!", "?"]:
                    text_sentences.write(f".\n")
                text_sentences.write("\n")
    get_hash_text(f"{out_dir}/tokenized.txt")


def deduplicate_corpus(corpus_as_dict: dict) -> dict:
    """
    Cette fonction crée le corpus de phrases et supprime la redondance.
    :param corpus_as_dict: 
    :return: 
    """
    total_number_of_sentences = 0
    kept_sentences = 0
    examples = {}
    for key, work in corpus_as_dict.items():
        examples[key] = []
        seen = []
        to_remove = []
        current_examples = []
        for index, example in enumerate(work['examples']):
            splitted = example["text"].split(".")
            as_tokens = []
            for sentence in splitted:
                total_number_of_sentences += 1
                stripped = sentence.strip()
                if stripped in seen and stripped != "" and len(stripped) > 30:
                    pass
                else:
                    kept_sentences += 1
                    seen.append(stripped)
                    if len(stripped) > 30:
                        as_tokens.append(stripped)
                        examples[key].append(stripped)
            old_example = example
            example = dict()
            example['original-text'] = old_example
            example['tokenized-no-redundancy'] = as_tokens
            example['words-tokenized'] = sum([len(sent.split()) for sent in example['tokenized-no-redundancy']])
            current_examples.append(example)
        work['examples'] = current_examples

    with open(f"{out_dir}/reduced_corpus.json", "w") as output_json:
        json.dump(corpus_as_dict, output_json)
    print(f"Total number of sentences: {total_number_of_sentences}")
    print(f"Kept sentences: {kept_sentences}")
    print(f"Ratio: {kept_sentences / total_number_of_sentences}")
    return corpus_as_dict, examples

def remove_accents(string):
    string = string.replace("á", "a")
    string = string.replace("é", "e")
    string = string.replace("í", "i")
    string = string.replace("ú", "u")
    string = string.replace("ó", "o")
    return string


def get_correct_wit_date(entry, cordemaforo_list):
    title = entry['title']
    # On filtre d'abord par la date de publication, pour ne pas avoir à trop parcourir de grandes listes
    edition_date = entry['publication-date']
    # On va utiliser une distance de levensthein pour identifier les oeuvres 
    corresponding_publications = [element for element in cordemaforo_list if element['DatEd'] == edition_date]
    try:
        corresponding_work = [element for element in corresponding_publications if
                              levenshtein_distance(remove_accents(element['Titulo']), title) < 4]
        if len(corresponding_work) > 1:
            # Des fois on matche plus d'une entrée, dans ce cas on va chercher l'exactitude du match avec les titres
            corresponding_work = [element for element in corresponding_publications if
                                  remove_accents(element['Titulo']) == title]
            if len(corresponding_work) > 1:
                # Quand le titre est identique, on ne peut rien faire
                return entry['year'], "N/A", False
            else:
                corresponding_work = corresponding_work[0]
                corresponding_witDate = corresponding_work['DatTest']
                correspondint_witDate_type = corresponding_work['DatTipo3']
                return corresponding_witDate, correspondint_witDate_type, True
        corresponding_work = corresponding_work[0]
    except IndexError:
        return entry['year'], "N/A", False
    corresponding_witDate = corresponding_work['DatTest']
    correspondint_witDate_type = corresponding_work['DatTipo3']
    return corresponding_witDate, correspondint_witDate_type, True


number_of_sentences = 0
word_count = 0
directory = sys.argv[1]
print(directory)

random.seed(123457)

corde_themes = {"11": "Lírica",
                "12": "Narrativa",
                "13": "Teatro",
                "14": "Didáctica",
                "15": "Ciencia y técnica",
                "16": "Sociedad",
                "17": "Religión",
                "18": "Prensa",
                "19": "Historia y documentos",
                "10": "Derecho",
                "21": "Lírica",
                "22": "Narrativa",
                "23": "Dramática"}

# Paramètres permettant de modifier les caractéristiques du corpus (diversité, moyennes, etc)
# Nombre d'exemples maximum par oeuvre
global_limit = 18
# Nombre d'exemples par oeuvre éditée à Madison
madison_limit = 12

reduce_dict = {"15": 15, "14": 100, "13": 100}
documentos_notariales_ratio = 0.15
codigos_legales_ratio = .20
minimal_words_per_work = 300
out_dir = sys.argv[1]
debug_val = bool(sys.argv[2])
in_dir = sys.argv[3]
# create_main_json_file()
# first_reduction_step()
balanced_corpus = balance_corpus()
deduplicated_corpus = deduplicate_corpus(balanced_corpus)
reduced_corpus = remove_small_works(balanced_corpus, minimal_words_per_work)
get_word_count_from_json(reduced_corpus)
examples = get_examples_from_json(reduced_corpus)
create_txt_files(examples)
print("Done")
