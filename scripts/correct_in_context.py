import json
import re
import sys


def read_to_json(path):
    with open(path, "r") as input_file:
       mods_dict = json.load(input_file)
    return mods_dict


def debug_msg(message):
    try:
        debug_dict[str(message)] += 1
    except KeyError:
        debug_dict[str(message)] = 1

def read_to_list_of_list(path):
    with open(path, "r") as input_file:
        list_of_list = [line.replace("\n", "").split("\t") for line in input_file.readlines()]
    return list_of_list



class Instruction:
    def __init__(self, instruction: list):
        print("Initiation")
        # Peut-être peut-on simplifier le tout en créant une fonction pour ne pas avoir à répéter ce code.
        self.instruction = instruction
        self.instructions_dict = {}
        for item, element in enumerate(self.instruction):
            print(element)
            self.process_instructions(element["match"], "match", item)
            self.process_instructions(element["left_context"], "left_context", item)
            self.process_instructions(element["right_context"], "right_context", item)
            self.process_instructions(element["replacement"], "replacement", item)

    def process_instructions(self, dictionnary, key, item):
        global_dict = {}
        global_dict[key] = {}
        if len(dictionnary) != 0:
            try:
                global_dict[key]["form"] = dictionnary["form"]
            except KeyError:
                global_dict[key]["form"] = None

            try:
                global_dict[key]["lemma"] = dictionnary["lemma"]
            except KeyError:
                global_dict[key]["lemma"] = None

            try:
                global_dict[key]["pos"] = dictionnary["pos"]
            except KeyError:
                global_dict[key]["pos"] = None

            try:
                global_dict[key]["morph"] = dictionnary["morph"]
            except KeyError:
                global_dict[key]["morph"] = None

        else:
            global_dict[key] = {}
        try:
            self.instructions_dict[item].update(global_dict)
        except KeyError:
            self.instructions_dict[item] = global_dict


def replace_analysis(analysis:list, instruction:dict):
    form, lemma, pos, morph = analysis
    if instruction["form"]:
        new_form = instruction["form"]
    else:
        new_form = form

    if instruction["lemma"]:
        new_lemma = instruction["lemma"]
    else:
        new_lemma = lemma

    if instruction["pos"]:
        new_pos = instruction["pos"]
    else:
        new_pos = pos

    if instruction["morph"]:
        new_morph = instruction["morph"]
    else:
        new_morph = morph

    return [new_form, new_lemma, new_pos, new_morph]

def create_tests(instruction, subinstruction, analyse):
    form, lemma, pos, morph = analyse
    try:
        if instruction[subinstruction]['form']:
            pattern_or_string = re.compile(f"^{instruction[subinstruction]['form']}$")
            if re.match(pattern_or_string, form):
                form_test = True
            else:
                form_test = False
        else:
            form_test = None
    except TypeError:
        form_test = None

    try:
        if instruction[subinstruction]['lemma']:
            pattern_or_string = re.compile(f"^{instruction[subinstruction]['lemma']}$")
            if re.match(pattern_or_string, lemma):
                lemma_test = True
            else:
                lemma_test = False
        else:
            lemma_test = None
    except TypeError:
        lemma_test = None

    try:
        if instruction[subinstruction]['pos']:
            pattern_or_string = re.compile(f"^{instruction[subinstruction]['pos']}$")
            if re.match(pattern_or_string, pos):
                pos_test = True
            else:
                pos_test = False
        else:
            pos_test = None
    except TypeError:
        pos_test = None

    try:
        if instruction[subinstruction]['morph']:
            pattern_or_string = re.compile(f"^{instruction[subinstruction]['morph']}$")
            if re.match(pattern_or_string, morph):
                morph_test = True
            else:
                morph_test = False
        else:
            morph_test = None
    except TypeError:
        morph_test = None

    return [test for test in [form_test, lemma_test, pos_test, morph_test] if test is not None]

def correct(filepath, modifications_path):
    print(filepath)
    list_of_analyses = read_to_list_of_list(filepath)
    modification_dict = read_to_json(modifications_path)
    to_process = Instruction(modification_dict)
    replacement_dict = to_process.instructions_dict
    corrected_dict = {}
    for index, analyse in enumerate(list_of_analyses):
        form, lemma, pos, morph = analyse
        treated = False
        for _, instruction in replacement_dict.items():
            if treated is False:
                test_list = create_tests(instruction, 'match', analyse)
                test_list = [test for test in test_list if test is not None]
                # On regarde déjà si l'analyse matche avec ce que l'on cherche
                if all([test == True for test in test_list]):
                    debug_msg(1)
                    # On va affiner et voir s'il y a du contexte à gauche
                    if instruction['left_context'] != {}:
                        debug_msg(2)
                        previous_analyse = list_of_analyses[index - 1]
                        test_list = create_tests(instruction, 'left_context', previous_analyse)
                        # Si ça matche à gauche on regarde s'il y a une instruction à droite
                        if all([test == True for test in test_list]):
                            debug_msg(3)
                            if instruction['right_context'] != {}:
                                debug_msg(4)
                                next_analysis = list_of_analyses[index + 1]
                                test_list = create_tests(instruction, 'right_context', next_analysis)
                                # Si ça matche aussi à droite on a identifié notre analyse en contexte et on remplace
                                if all([test == True for test in test_list]):
                                    debug_msg(5)
                                    corrected_dict[index] = replace_analysis(analyse, instruction['replacement'])
                                    treated = True
                                # Si ça ne matche pas à droite ce n'est pas le bon contexte, on renvoie la ligne
                                else:
                                    debug_msg(6)
                                    corrected_dict[index] = analyse
                            # Si il n'y a pas d'indication de contexte droit, on remplace puisque ça matche
                            else:
                                debug_msg(7)
                                corrected_dict[index] = replace_analysis(analyse, instruction['replacement'])
                                treated = True
                        # Si ça ne matche pas à gauche on renvoie l'analyse
                        else:
                            debug_msg(8)
                            corrected_dict[index] = analyse
                    # Si à gauche on n'a pas d'instruction on regarde à droite
                    elif instruction['right_context'] != {}:
                        debug_msg(9)
                        next_analysis = list_of_analyses[index + 1]
                        test_list = create_tests(instruction, 'right_context', next_analysis)
                        if all([test == True for test in test_list]):
                            debug_msg(10)
                            # Si ça matche à droite on remplace l'analyse
                            corrected_dict[index] = replace_analysis(analyse, instruction['replacement'])
                            treated = True
                        else:
                            # Sinon on renvoie l'analyse telle quelle
                            debug_msg(11)
                            corrected_dict[index] = analyse
                    else:
                        # Si il n'y a ni instruction droite ni gauche on teste l'analyse
                        debug_msg(12)
                        if all([test == True for test in test_list]):
                            debug_msg(13)
                            # Si ça matche l'instruction on remplace
                            corrected_dict[index] = replace_analysis(analyse, instruction['replacement'])
                            treated = True
                        else:
                            # Sinon on renvoie l'analyse
                            debug_msg(14)
                            corrected_dict[index] = analyse
                else:
                    debug_msg(15)
                    corrected_dict[index] = analyse

    print(len(corrected_dict))
    print(len(list_of_analyses))
    # On finit par ajouter une information  "à vérifier" si forme et lemme sont identiques:
    for index, line in corrected_dict.items():
        form, lemma, pos, morph = line
        if form == lemma and morph != "True" and "False":
            morph = morph + "+True"
        corrected_dict[index] = [form, lemma, pos, morph]

    with open("/home/mgl/Documents/test_lemma.txt", "w") as output_test_lemma_list:
        for index, line in corrected_dict.items():
            output_test_lemma_list.write("\t".join(line) + "\n")

    with open("/home/mgl/Documents/orig_lemma.txt", "w") as output_orig_lemma_list:
        for index, line in corrected_dict.items():
            output_orig_lemma_list.write("\t".join(line) + "\n")




if __name__ == '__main__':
    debug = True
    debug_dict = {}
    path_to_file = sys.argv[1]
    path_to_instructions = sys.argv[2]
    correct(path_to_file, path_to_instructions)
    print(debug_dict)