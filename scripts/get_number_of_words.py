import glob
import sys
import json
import tqdm


def get_number(examples):

    print("Total word count")
    word_count = 0
    for sentence in examples:
        word_count += len(sentence.split())
    print(word_count)

    words_per_example = round(word_count / len(examples))
    print(f"Mean number of words per example: {str(words_per_example)}")


    print("Example number before reduction:")
    print(len(examples))
    examples_no_spaces = [example.replace(" ", "") for example in examples]
    examples_no_spaces = list(set(examples_no_spaces))
    print("Example number after reduction:")
    print(len(examples_no_spaces))
    print(f"Aprox. number of words after reduction: {str(round(len(examples_no_spaces) * words_per_example))}")


    print("Example number before reduction (no space removal):")
    print(len(examples))
    examples_spaces = [example for example in examples]
    examples_spaces = list(set(examples_spaces))
    print("Example number after reduction  (no space removal):")
    print(len(examples_spaces))
    print(f"Aprox. number of words after reduction (no space removal): {str(round(len(examples_spaces) * words_per_example))}")


    print("Word count with no redundancy")
    word_count = 0
    examples_reduces = list(set(examples))
    for sentence in examples_reduces:
        word_count += len(sentence.split())
    print(word_count)




directory = sys.argv[1]
list_of_sentences_no_madison = []
list_of_sentences = []
for file in tqdm.tqdm(glob.glob(f"{directory}/*.json")):
    with open(file, "r") as input_file:
        as_json = json.load(input_file)
    try:
        for example in as_json.values():
            list_of_sentences.append(example['text'])
            if not "Madison" in example['published-by']:
                list_of_sentences_no_madison.append(example['text'])

    except AttributeError:
        continue

print("Without Madison texts:")
get_number(list_of_sentences_no_madison)
print("Total corpus including Madison")
get_number(list_of_sentences)


