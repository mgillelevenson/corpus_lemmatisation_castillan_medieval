import json
import os
import sys
import re
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import OrderedDict


def value_to_proportion(value, list_of_values):
    return (value / sum(list_of_values)) * 100


def plot_data(data: dict, x_caption, y_caption, plot_title, image_path):
    # On doit d'abord s'assurer que les abscisses sont bien ordonées
    # On travaille en pourcentages
    list_val = [(x, value_to_proportion(y, data.values())) for x, y in data.items()]
    list_val.sort(key=lambda x: x[0])
    x_val, y_val = zip(*list_val)
    y_val = [int(round(val, 1)) for val in y_val]
    plt.style.use('_mpl-gallery')
    # plot
    fig, ax = plt.subplots(figsize=(15, 15))
    ax.bar(x_val, y_val, width=1, edgecolor="white", linewidth=0.7)

    ax.set(xlim=(-1, len(x_val)), xticks=np.arange(0, len(x_val)),
           ylim=(0, max(y_val) + 1), yticks=range(0, max(y_val) + 5, 5))

    # naming the x axis
    plt.xlabel(x_caption)
    # naming the y axis
    plt.ylabel(y_caption)
    ax.set_xticklabels(x_val, rotation=90)
    # giving a title to my graph
    plt.title(plot_title)

    # function to show the plot
    plt.subplots_adjust()
    plt.savefig(image_path, bbox_inches='tight', dpi=100)


def create_plots(dictionnary):
    theme_dict = {}
    subtheme_dict = {}
    normDate_dict = {}
    centuryDict = {}
    CordeCenturyDict = {}
    EditionDateDict = {}
    # Ici pour l'instant on a des statistiques sur les exemples et pas sur les oeuvres. Travailler à ça.
    for identifier, values in dictionnary.items():
        for example in values['examples']:
            # On va créer des dictionnaires en comptant le nombre d'occurrence par catégorie, pour faire
            # des graphiques
            # On commence par les thèmes
            try:
                theme_dict[values['theme']] += example['words-tokenized']
            except KeyError:
                theme_dict[values['theme']] = example['words-tokenized']

            # Puis les sous-thèmes
            try:
                subtheme_dict[values['subtheme']] += example['words-tokenized']
            except KeyError:
                subtheme_dict[values['subtheme']] = example['words-tokenized']

            # Les dates
            try:
                normDate_dict[values['wit-date-range']] += example['words-tokenized']
            except KeyError:
                normDate_dict[values['wit-date-range']] = example['words-tokenized']

            # Et enfin les siècles
            try:
                EditionDateDict[values['edition-date-range']] += example['words-tokenized']
            except KeyError:
                EditionDateDict[values['edition-date-range']] = example['words-tokenized']

            try:
                CordeCenturyDict[values['century']] += example['words-tokenized']
            except KeyError:
                CordeCenturyDict[values['century']] = example['words-tokenized']

            try:
                centuryDict[values['wit-date-century']] += example['words-tokenized']
            except KeyError:
                centuryDict[values['wit-date-century']] = example['words-tokenized']
        
    plot_data(data=theme_dict,
              x_caption="Thème",
              y_caption="% occurrences",
              plot_title='Distribution du corpus en volume de mots par thème CORDE',
              image_path=f"{out_dir}/stats/by_theme.png")

    plot_data(data=subtheme_dict,
              x_caption="Sous-thème",
              y_caption="% occurrences",
              plot_title='Distribution du corpus en volume de mots par sous-thème CORDE',
              image_path=f"{out_dir}/stats/by_subtheme.png")

    plot_data(data=normDate_dict,
              x_caption="Date",
              y_caption="% occurrences",
              plot_title='Distribution du corpus en volume de mots par quart de siècle',
              image_path=f"{out_dir}/stats/by_date_range.png")

    plot_data(data=centuryDict,
              x_caption="Siècle",
              y_caption="% occurrences",
              plot_title='Distribution du corpus en volume de mots par siècle (dates corrigées)',
              image_path=f"{out_dir}/stats/by_century_cordemaforo.png")

    plot_data(data=EditionDateDict,
              x_caption="Siècle",
              y_caption="% occurrences",
              plot_title="Distribution du corpus en volume de mots par date de l'édition",
              image_path=f"{out_dir}/stats/by_edition_date.png")

    plot_data(data=CordeCenturyDict,
              x_caption="Siècle",
              y_caption="% occurrences",
              plot_title='Distribution du corpus en volume de mots par siècle (dates CORDE)',
              image_path=f"{out_dir}/stats/by_century_corde.png")


def create_readme(stats: dict):
    # plt.plot([1, 2, 3, 4])
    # plt.ylabel('some numbers')
    # plt.show()
    # On écrit le README
    df = pd.read_csv(f"{out_dir}/works.tsv", sep="\t")
    with open(f"../README.md", "w") as md:
        md.write("# Corpus d'annotation lexico-grammaticale du castillan médiéval\n\n")
        md.write("Ce dépôt contient les données d'un corpus de lemmatisation, "
                 "d'annotation grammaticale et morphologique en castillan médiéval (1220-1500). Il vise à terme, "
                 "après correction, à l'entraînement d'un modèle d'annotation lexico-grammatical performant.\n")
        md.write("Ce corpus n'a pas vocation à remplacer les grands corpus linguistiques (CORDE, OSTA) "
                 "et ne contient que des extraits d'oeuvres. Le corpus d'entraînement per se, au format .txt,"
                 " contient les exemples tokénisés et triés aléatoirement. "
                 "Les informations de provenance de chaque exemple en sont absentes.\n")
        md.write("## Description du corpus\n\n")
        md.write(f"Le corpus se veut représentatif du corpus général du castillan bas-médiéval, "
                 "du point de vue générique en particulier. Outre les oeuvres littéraires (oeuvres légales incluses), "
                 "il contient environ un tiers de données issues de documents historiques. "
                 f"La description du contenu du corpus est proposée ci-dessous. Il contient en l'état {comma_me(stats['corpus_size'])} "
                 f"mots (ponctuation incluse), {stats['examples_number']} exemples provenant de"
                 f" {stats['work_number']} oeuvres différentes produites entre 1200 (essentiellement 1250) et 1500. La typologie générique"
                 f" utilisée provient de CORDE. La diversité du corpus est aussi garantie du point de vue des normes de transcription qui diffèrent"
                 f" selon l'édition récupérée.\n\n")
        md.write("### Description quantitative\n\n")
        md.write(f"La taille du vocabulaire du corpus est de {comma_me(stats['vocabulary_size'])}, corpus qui "
                 f"compte {comma_me(stats['number_of_forms'])} formes distinctes.\n\n")
        md.write("**NB:** Freeling opère une re-tokénisation de certaines formes "
                 "(les numéraux par exemples) qui ne peut être empêchée. "
                 "Le nombre final de mots dans le corpus lemmatisé varie "
                 "donc très légèrement par rapport à celui indiqué ci-dessus.\n\n")
        md.write(
            "**Caveat:** La datation de CORDE doit être sujette à caution; lors de la publication du corpus final les informations de datation seront amendées à l'aide de la table CORDEMÁFORO produite par Javier Rodríguez Molina et  Álvaro Octavio de Toledo y Huerta (Rodríguez Molina, Javier; Octavio de Toledo y Huerta, Álvaro. «La imprescindible distinción entre texto y testimonio: el CORDE y los criterios de fiabilidad lingüística». Scriptum digital. Revista de corpus diacrònics i edició digital en Llengües iberoromàniques, 2017, Núm. 6, p. 5-68, https://raco.cat/index.php/scriptumdigital/article/view/329258).\n\n")
        df.fillna('', inplace=True)
        df.to_markdown(buf=md, index=False)


def comma_me(amount):
    # https://stackoverflow.com/a/3154485
    amount = str(amount)
    orig = amount
    pattern = re.compile(r"^(-?\d+)(\d{3})")
    new = re.sub(pattern, '\g<1>.\g<2>', amount)
    if orig == new:
        return new
    else:
        return comma_me(new)


def create_stats_and_metadata(reduced_dict, lemmatized_file):
    with open(f"{out_dir}/works.tsv", "w") as list_of_works:
        list_of_works.write(
            "ID\tTitre\tDate (corde)\tDate normalisée (corde)\tDate corrigée ?\tDate (amendée)\tDate normalisée ("
            "amendée)\tThème\tSous-thème\tÉditeur·ice\tDate de l'édition\tNb. de phrases\tNb. de mots\n")

        # On va compter le nombre d'exemple par titre
        titles_dict = dict()
        for key, item in reduced_dict.items():
            titles_dict[key] = len(reduced_dict[key]['examples'])
        # Et on crée la liste finale
        output_dict = OrderedDict()
        total_word_number = 0
        total_work_number = 0
        for identifier, item in reduced_dict.items():
            word_number = 0
            total_work_number += len(item)
            for it in item['examples']:
                word_number += it["words-tokenized"]
                total_word_number += it["words-tokenized"]
                output_dict[identifier] = (
                    item['title'], item['year'], item['date_range'], item['date-amended'], item['wit-date'],
                    item['wit-date-range'], item['theme'], item["subtheme"], item['published-by'],
                    item['publication-date'],
                    str(titles_dict[identifier]),
                    str(word_number))
        output_list = [(ident, titre, annee_corde, annee_norm_corde, date_amendee, annee_cordemaforo,
                        annee_norm_cordemaforo, theme, subtheme, editeur, date_edition, nombre_exemples, nombre_mots)
                       for
                       ident, (
                           titre, annee_corde, annee_norm_corde, date_amendee, annee_cordemaforo,
                           annee_norm_cordemaforo,
                           theme, subtheme, editeur, date_edition, nombre_exemples, nombre_mots) in
                       output_dict.items()]
        output_list.sort(key=lambda x: (int(x[-1]), x[0]), reverse=True)
        for entry in output_list:
            list_of_works.write("\t".join([str(item) for item in entry]) + "\n")

    with open(lemmatized_file, "r") as input_file:
        lines = [line.replace("\n", " ") for line in input_file.readlines()[1:]]
        analyses = [line.split(" ") for line in lines if len(line.split()) == 4]
    forms_dict = {}
    lemma_dict = {}

    forms = []
    for analyse in analyses:
        try:
            form, lemma, pos, *_ = analyse
        except ValueError:
            continue
        try:
            forms_dict[form] += 1
        except KeyError:
            forms_dict[form] = 1
        try:
            lemma_dict[lemma] += 1
        except KeyError:
            lemma_dict[lemma] = 1
        forms.append(form)
    number_of_forms = len(forms_dict)
    vocabulary = len(lemma_dict)
    corpus_size = len(analyses)

    # On sauvegarde les dictionnaires
    with open(f"{out_dir}/stats/words_stats.json", "w") as output_forms:
        json.dump(forms_dict, output_forms)

    with open(f"{out_dir}/stats/lemma_stats.json", "w") as output_lemmas:
        json.dump(lemma_dict, output_lemmas)

    stats = {"word_number": comma_me(total_word_number),
             "work_number": comma_me(len(reduced_dict)),
             "examples_number": comma_me(total_work_number),
             "corpus_size": corpus_size,
             "number_of_forms": number_of_forms,
             "vocabulary_size": vocabulary
             }
    with open(f"{out_dir}/stats/numerical_stats.json", "w") as output_stats:
        json.dump(stats, output_stats)
    return stats


if __name__ == '__main__':
    reduced_corpus = sys.argv[1]
    out_dir = sys.argv[3]
    os.makedirs(f"{out_dir}/stats", exist_ok=True)
    lemmatized_path = sys.argv[2]
    with open(reduced_corpus, "r") as reduced:
        reduced_dict = json.load(reduced)
    stats = create_stats_and_metadata(reduced_dict, lemmatized_path)
    create_readme(stats)
    matplotlib.use('TkAgg')
    create_plots(reduced_dict)
